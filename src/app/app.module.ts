import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import {RouterModule} from "@angular/router";
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpModule } from '@angular/http'; 

import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';

import { Tabs} from '@ionic/angular';
import { IonicSelectableModule } from 'ionic-selectable';
import { IonicStorageModule } from '@ionic/storage'
import { AngularFireMessagingModule } from '@angular/fire/messaging';

var config = {
  apiKey: "AIzaSyCuiItdZw6wdd_OXqNLE0QIuDUJuUi29D0",
  authDomain: "projectmobiledev.firebaseapp.com",
  databaseURL: "https://projectmobiledev.firebaseio.com",
  projectId: "projectmobiledev",
  storageBucket: "projectmobiledev.appspot.com",
  messagingSenderId: "545230077562"
};

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(),HttpModule, AppRoutingModule, AngularFireModule.initializeApp(config), AngularFireDatabaseModule, AngularFireMessagingModule,
    AngularFireAuthModule, RouterModule, IonicSelectableModule, IonicStorageModule.forRoot()],
  providers: [
    StatusBar,
    SplashScreen,
    Tabs,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
