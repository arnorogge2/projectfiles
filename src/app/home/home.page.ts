import { Component } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';
import { Router } from '@angular/router';
import { NavController, Tabs } from '@ionic/angular';
import { AngularFireDatabase } from '@angular/fire/database';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss']
})


export class HomePage {
  constructor(public afAuth: AngularFireAuth, private nav: NavController, private router: Router, public tabs: Tabs, public fdb: AngularFireDatabase) {
  }

  writeDatabase(newUser) {
    this.fdb.list("/users/").set(newUser.uid, {
      displayName: newUser.displayName,
      email: newUser.email,
      movies: ',',
      series: ',',
    });
  }
  Facebooklogin() {
    this.afAuth.auth.signInWithPopup(new auth.FacebookAuthProvider()).then((result) => {
      if (result.additionalUserInfo.isNewUser) {
        var newUser = result.user;
        this.writeDatabase(newUser);
      }
    });
  }
  Googlelogin() {
    this.afAuth.auth.signInWithPopup(new auth.GoogleAuthProvider()).then((result) => {
      if (result.additionalUserInfo.isNewUser) {
        var newUser = result.user;
        this.writeDatabase(newUser);
      }
    });
  }
}


