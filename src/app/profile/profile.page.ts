import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';
import { AngularFireDatabase } from '@angular/fire/database';
import { Http, Headers } from '@angular/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  followedSeries: any;
  followedSeriedArray: any;
  followedMovies: any;
  followedMoviedArray: any;

  constructor(public afAuth: AngularFireAuth, private fdb: AngularFireDatabase, public http: Http, public rtr: Router) {
    this.afAuth.auth.onAuthStateChanged(user => {
      if (user) {
        var followedSeriedArray = [];
        var followedMoviedArray = [];
        var ref = this.fdb.database.ref("users/" + this.afAuth.auth.currentUser.uid);
        ref.once("value")
          .then(snapshot => {
            var childKey = snapshot.child("series").val();
            this.followedSeries = (childKey.toString()).split(',');
            var i;
            for (i = 0; i < this.followedSeries.length; i++) {
              if (this.followedSeries[i] != "") {
                this.http.get('https://api.themoviedb.org/3/tv/' + this.followedSeries[i] + '?api_key=37ccb790eeea88e5d24a68672814040a&language=en-US').map(res => res.json()).subscribe(data => {
                  followedSeriedArray.push(data);
                  this.followedSeriedArray = followedSeriedArray;
                }, err => {
                  console.log(err);
                });
              }
            }


            var childKey = snapshot.child("movies").val();
            this.followedMovies = (childKey.toString()).split(',');
            var i;
            for (i = 0; i < this.followedMovies.length; i++) {
              if (this.followedMovies[i] != "") {
                this.http.get('https://api.themoviedb.org/3/movie/' + this.followedMovies[i] + '?api_key=37ccb790eeea88e5d24a68672814040a&language=en-US').map(res => res.json()).subscribe(data => {
                  followedMoviedArray.push(data);
                  this.followedMoviedArray = followedMoviedArray;
                }, err => {
                  console.log(err);
                });
              }
            }



          });
      }
    });

  }
  logout() {
    this.afAuth.auth.signOut();
    this.rtr.navigateByUrl('/login');
  }

  clickedLogin() {
    this.rtr.navigateByUrl('/tabs/(login:login)');
  }

  ngOnInit() {

  }
}
