import { Component, OnInit } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { NavController } from '@ionic/angular';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
@Component({
  selector: 'app-upcoming',
  templateUrl: './upcoming.page.html',
  styleUrls: ['./upcoming.page.scss'],
})
export class UpcomingPage implements OnInit {
  segmentChanged(ev: any) {
    this.displayUpcoming(ev.target.value);
  }
  url: string;
  data: string;
  MediaType: any;
  constructor(public http: Http, public navCtrl: NavController, private storage: Storage, public rtr: Router) { }
  displayUpcoming(eventValue) {
    if (eventValue == "Series") {
      this.http.get('https://api.themoviedb.org/3/tv/airing_today?api_key=37ccb790eeea88e5d24a68672814040a&language=en-US&page=1').map(res => res.json())
        .subscribe(data => {
          var old = JSON.stringify(data.results).replace(/null/g, '"/9lfT4FfPtwpu0k0MTZqFchvQNsR.jpg"');
          this.data = JSON.parse(old);
        }, err => {
          console.log(err);
        });
    }
    else {
      this.http.get('https://api.themoviedb.org/3/movie/upcoming?api_key=37ccb790eeea88e5d24a68672814040a&language=en-US&page=1').map(res => res.json())
        .subscribe(data => {
          var old = JSON.stringify(data.results).replace(/null/g, '"/9lfT4FfPtwpu0k0MTZqFchvQNsR.jpg"');
          this.data = JSON.parse(old);
        }, err => {
          console.log(err);
        });
    }
  }
  ngOnInit() {
    this.MediaType = "Movies";
    this.http.get('https://api.themoviedb.org/3/movie/upcoming?api_key=37ccb790eeea88e5d24a68672814040a&language=en-US&page=1').map(res => res.json())
      .subscribe(data => {
        var old = JSON.stringify(data.results).replace(/null/g, '"/9lfT4FfPtwpu0k0MTZqFchvQNsR.jpg"');
        this.data = JSON.parse(old);
      }, err => {
        console.log(err);
      });
  }
  clickedSearch() {
    this.rtr.navigateByUrl('/tabs/(information:information)');
  }

  open(event, item) {
    var mediatype = this.MediaType;
    event.stopPropagation();
    this.storage.set('idDisplay', item.id);
    if (mediatype == "Series")
    {
      mediatype = "Serie";
    }
    else
    {
      mediatype = "Movie";
    }
    this.storage.set('typeDisplay', mediatype);
    console.log(mediatype);
    window.location.reload();
    this.rtr.navigateByUrl('/tabs/(about:about)');
  }

}
